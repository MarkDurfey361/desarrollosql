/** Create ADDRESS UDT ***/
/*CREA UN OBJETO TIPO ADDRESS
CREATE TYPE ADDRESS AS OBJECT
( 
  street        VARCHAR(60),
  city          VARCHAR(30),
  state         CHAR(2),
  zip_code      CHAR(5)
);

 Create PERSON UDT containing an embedded ADDRESS UDT
CREATE TYPE PERSON AS OBJECT
( 
  name    VARCHAR(30),
  ssn     NUMBER,
  addr    ADDRESS
);*/

/*** Create a typed table for PERSON objects 
CREATE TABLE persons OF PERSON;*/

/*** Create a relational table with two columns that are REFs
     to PERSON objects, as well as a column which is an Address ADT.
CREATE TABLE  employees
( 
  empnumber            INTEGER PRIMARY KEY,
  person_data     REF  PERSON,
  manager         REF  PERSON,
  office_addr          ADDRESS,
  salary               NUMBER
);
*/

/*NOTA: COMENT� LA DEFINICI�N DE LOS OBJETOS, PORQUE ME DABA PROBLEMAS AL CREARLOS
A�N COLOCANDO EL ";" AL FINAL. ES PROBLEMA DEL ENTORNO, NO DE ORACLE MISMO.*/

/*** Insert some data--2 objects into the persons typed table ***/
INSERT INTO persons VALUES (
            PERSON('Wolfgang Amadeus Mozart', 123456,
               ADDRESS('Am Berg 100', 'Salzburg', 'AT','10424')));

INSERT INTO persons VALUES (
            PERSON('Ludwig van Beethoven', 234567,
               ADDRESS('Rheinallee', 'Bonn', 'DE', '69234')));
               
/** Put a row in the employees table **/
INSERT INTO employees (empnumber, office_addr, salary) VALUES (
            1001,
            ADDRESS('500 Oracle Parkway', 'Redwood Shores', 'CA', '94065'),
            50000);

/** Set the manager and PERSON REFs for the employee **/
UPDATE employees 
   SET manager =  
       (SELECT REF(p) FROM persons p WHERE p.name = 'Wolfgang Amadeus Mozart');

UPDATE employees 
   SET person_data =  
       (SELECT REF(p) FROM persons p WHERE p.name = 'Ludwig van Beethoven');
       
    select PERSON_DATA[0] FROM employees;
       
/* INSERT AGREGANDO EL PERSON Y EL ADDRESS DIRECTAMENTE */

--Primero agrego una nueva persona con su addres
INSERT INTO persons VALUES (
            PERSON('Marco Antonio', 987654,
               ADDRESS('Sta Mar�a', 'Apaxco', 'MX','55667')));
               
               
/* LUEGO INSERTO LOS DATOS */
INSERT INTO employees (empnumber, person_data, manager, office_addr, salary) VALUES (
            101,
            (SELECT REF(p) FROM persons p WHERE p.name = 'Marco Antonio'),
            (SELECT REF(p) FROM persons p WHERE p.name = 'Ludwig van Beethoven'),
            ADDRESS('San Juan', 'Zumpango', 'MX', '59847'),
            50000);
               