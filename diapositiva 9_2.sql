/* ROLLUP */

/* Muetsra department_id, job_id y la suma del salario */
SELECT department_id, job_id, as employeess, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY ROLLUP (department_id, job_id);

/* CUBE */
/* Realiza las combinaciones de department_id y job_id y en ambos casos muestra la suma del salario */
SELECT department_id, job_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY CUBE (department_id, job_id);

/* GROUPING SETS */

SELECT department_id, job_id, manager_id, SUM(salary)
FROM employees
WHERE department_id < 50
GROUP BY GROUPING SETS
((job_id, manager_id), (department_id, job_id), (department_id, manager_id));

SELECT department_id, job_id, sum(salary),
grouping(department_id) as "dept sub total",
grouping(job_id) as "job sub total"
from employees
where department_id < 50
group by cube (department_id, job_id);