SELECT
    MAX(salary)
FROM
    employees; --Obtiene el m�ximo
    
    
SELECT
    MIN(salary)
FROM
    employees;--Obtiene el m�nimo
    
    
SELECT
    SUM(salary)
FROM
    employees; --Suma de los valores
    
    
SELECT
    AVG(salary)
FROM
    employees; --promedio de todos
    
    
SELECT
    COUNT(salary)
FROM
    employees; --conteo de registros


SELECT
    MAX(hire_date)
FROM
    employees; --Fecha de ultima contrataci�n
    
    
SELECT
    SUM(salary)
FROM
    employees
WHERE
    department_id = 90;

/* Redondeo del promedio del salario depto 90 */
SELECT
    round(AVG(salary), 2) AS "Salario Promedio Redondeado"
FROM
    employees
WHERE
    department_id = 90;

/* Obtiene el last name de quien tiene el salario m�s alto */
SELECT
    last_name AS "Se le paga m�s"
FROM
    employees
WHERE
    salary = (
    /* Obtiene el m�ximo salario de los empleados */
        SELECT
            MAX(salary)
        FROM
            employees
    );

/* Obtiene la suma de los salarios que no se repiten del departamento 90 */
SELECT
    SUM(DISTINCT salary)
FROM
    employees
WHERE
    department_id = 90;

/* Saca el promedio de las comisiones, pero primero asigna cero
a los valores NULL*/
SELECT
    AVG(nvl(commission_pct, 0)) AS "Promedio Comisi�n"
FROM
    employees;

/* Salario promedio por departamento */
SELECT
    department_id AS "Departamento",
    AVG(salary) AS "Sueldo Promedio"
FROM
    employees
GROUP BY
    department_id
ORDER BY
    department_id;

/* Salario m�ximo por despartamento */
SELECT
    department_id AS "Departamento", 
    MAX(salary) AS "Sueldo M�ximo"
FROM
    employees
GROUP BY
    department_id
ORDER BY
    department_id;

/* Cu�ntos empleados hay por puesto */
SELECT
    job_id,
    COUNT(*)
FROM
    employees
GROUP BY
    job_id;

/* Cu�ntos empleados hay por puesto */
SELECT
    job_id AS "Puesto",
    COUNT(department_id) AS "Empleados"
FROM
    employees
GROUP BY
    job_id;

/*Agrupar por departemntoid y despues subgrupos por cada jobid y saber 
cuantos empleados hay por cada combinacion*/
SELECT
    department_id,
    job_id,
    COUNT(*)
FROM
    employees
WHERE
    department_id > 40
GROUP BY
    department_id,
    job_id
ORDER BY
    department_id,
    job_id;

/*
Muestra el id de departamento y el salario promedio de cada
uno de ellos. Los ordena de mayor a menor.
*/
SELECT
    department_id,
    AVG(salary) AS promedio
FROM
    employees
GROUP BY
    department_id
ORDER BY
    promedio DESC;

/*
Muestra el id de departamento y el salario promedio m�s alto
de ellos.
*/
SELECT
    department_id,
    AVG(salary) AS promedio
FROM
    employees
WHERE
    ROWNUM = 1
GROUP BY
    department_id
ORDER BY
    promedio DESC;

/*
Muestra s�lo el salario promedio m�s alto de los deptos.
SIN MANIPULAR EL MAX(AVG())
*/
SELECT
    MAX(AVG(salary)) AS MaxProm
FROM
    employees
WHERE
    ROWNUM = 1
GROUP BY
    department_id
ORDER BY
    MaxProm;
    
/*NOTA: AL PRINCIPIO UTILIZABAMOS COLOCAR "HR.COL_NAME" 
YA QUE DEB�AMOS INDICAR AL USUARIO AL CU�L HAC�AMOS REFERENCIA.
AHORA YA NO, PUESTO QUE HEMOS INICIADO LA SESI�N EN DICHO USUARIO.*/
